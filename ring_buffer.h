/**
 * @file
 * @author Martin Stejskal
 * @brief Ring buffer utility for embedded platforms
 */
#ifndef __RING_BUFF_H__
#define __RING_BUFF_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief Generic structure for ring buffer
 */
typedef struct {
  // Pointer to buffer
  void *pv_buffer;

  // Total buffer size in Bytes
  uint16_t u16_buffer_size_bytes;

  // Item size in Bytes
  uint8_t u8_item_size_bytes;

  // Read index
  uint16_t u16_read_idx;

  // Write index
  uint16_t u16_write_idx;
} ts_ring_buffer;

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Push data into ring buffer
 * @param[in, out] ps_ring_buffer Pointer to ring buffer structure
 * @param[in] pv_data Pointer to input data
 */
void ring_buffer_push(ts_ring_buffer *ps_ring_buffer, const void *pv_data);

/**
 * @brief Pull data from ring buffer
 * @param[in, out] ps_ring_buffer Pointer to ring buffer structure
 * @param[out] pv_data Data will be pulled into this memory space
 */
void ring_buffer_pull(ts_ring_buffer *ps_ring_buffer, void *pv_data);
// ========================| Middle level functions |=========================
/**
 * @brief Reset ring buffer include content
 * @param[in, out] ps_ring_buffer Pointer to ring buffer structure
 */
void ring_buffer_clean(ts_ring_buffer *ps_ring_buffer);

/**
 * @brief Read out latest value while not touch read index
 * @param[in, out] ps_ring_buffer Pointer to ring buffer structure
 * @param[out] pv_data Data will be pulled into this memory space
 */
void ring_buffer_pull_latest(ts_ring_buffer *ps_ring_buffer, void *pv_data);

/**
 * @brief Read item from ring buffer without touching read index
 * @param[in, out] ps_ring_buffer Pointer to ring buffer structure
 * @param[out] pv_data Data will be pulled into this memory space
 * @param[in] u16_relative_idx Relative index where 0 means "oldest value"
 */
void ring_buffer_pull_relative(ts_ring_buffer *ps_ring_buffer, void *pv_data,
                               const uint16_t u16_relative_idx);
// ==========================| Low level functions |==========================
/**
 * @brief Set read index to "beginning" relative to write index
 * @param[in, out] ps_ring_buffer Pointer to ring buffer structure
 */
void ring_buffer_reset_read_idx(ts_ring_buffer *ps_ring_buffer);

/**
 * @brief Returns number of items in ring buffer
 * @param[in] ps_ring_buffer Pointer to ring buffer structure
 * @return Number of items in given ring buffer
 */
uint16_t ring_buffer_get_num_of_items(ts_ring_buffer *ps_ring_buffer);
#endif  // __RING_BUFF_H__
